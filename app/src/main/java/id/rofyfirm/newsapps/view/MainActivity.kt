package id.rofyfirm.newsapps.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.rofyfirm.newsapps.adapter.MainAdapter
import id.rofyfirm.newsapps.databinding.ActivityMainBinding
import id.rofyfirm.newsapps.response.HeadlineResponse
import id.rofyfirm.newsapps.vm.MainVm
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private var listModel = ArrayList<HeadlineResponse.Article>()
    private lateinit var listAdapter: MainAdapter

    private val binding: ActivityMainBinding by lazy{ ActivityMainBinding.inflate(layoutInflater) }

    private val mainVm: MainVm by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        init()
    }

    private fun init(){
//        mainVm.getValue()
        mainVm.getDataFromRetrofit().observe(this) {
            for (i in it.articles?.indices!!){
                listModel.add(it.articles[i])
            }

            listAdapter = MainAdapter(listModel)

            binding.recycler.apply {
                listAdapter.notifyDataSetChanged()
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = listAdapter
            }
        }
    }
}