package id.rofyfirm.newsapps.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.rofyfirm.newsapps.R
import id.rofyfirm.newsapps.databinding.ActivityNewsBinding

class NewsActivity : AppCompatActivity() {

    private val binding: ActivityNewsBinding by lazy { ActivityNewsBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.toolbar.title = "Berita"
        binding.toolbar.navigationIcon = getDrawable(R.drawable.ic_baseline_arrow_back_24)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        init()
    }

    private fun init(){

    }
}