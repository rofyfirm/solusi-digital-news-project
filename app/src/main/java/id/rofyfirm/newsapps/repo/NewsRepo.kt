package id.rofyfirm.newsapps.repo

import androidx.lifecycle.LiveData
import id.rofyfirm.newsapps.base.BaseRepo
import id.rofyfirm.newsapps.network.RequestApi
import id.rofyfirm.newsapps.response.NewsDetailResponse

class NewsRepo(request: RequestApi):
    BaseRepo<NewsDetailResponse>(request){


    override fun loadData(): LiveData<NewsDetailResponse> {
        return fetchData {
            request.detailNews()
        }
    }
}