package id.rofyfirm.newsapps.repo

import androidx.lifecycle.LiveData
import id.rofyfirm.newsapps.base.BaseRepo
import id.rofyfirm.newsapps.network.RequestApi
import id.rofyfirm.newsapps.response.HeadlineResponse

class MainRepo(request: RequestApi):
    BaseRepo<HeadlineResponse>(request){

    override fun loadData(): LiveData<HeadlineResponse> {
        return fetchData {
            request.topHeadlines("id")
        }
    }
}