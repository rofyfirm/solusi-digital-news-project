package id.rofyfirm.newsapps.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.rofyfirm.newsapps.databinding.ItemNewsBinding
import id.rofyfirm.newsapps.response.HeadlineResponse

class MainAdapter (
    private var listModel: List<HeadlineResponse.Article> = arrayListOf()):
    RecyclerView.Adapter<MainAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listModel[position])
    }

    override fun getItemCount(): Int {
        return listModel.size
    }

    inner class ViewHolder(val binding: ItemNewsBinding): RecyclerView.ViewHolder(binding.root){
        @SuppressLint("CheckResult")
        fun bind(data: HeadlineResponse.Article){
            binding.content.text = data.content
            binding.title.text = data.title
            binding.source.text = data.source.name
            Glide.with(binding.root).load(data.urlToImage).into(binding.image)
        }
    }
}