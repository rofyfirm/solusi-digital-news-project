package id.rofyfirm.newsapps.vm

import androidx.lifecycle.LiveData
import id.rofyfirm.newsapps.base.BaseVm
import id.rofyfirm.newsapps.repo.MainRepo
import id.rofyfirm.newsapps.response.HeadlineResponse

class MainVm(private val repo: MainRepo):
    BaseVm<HeadlineResponse>(){

    private lateinit var data: LiveData<HeadlineResponse>

    override fun getStatusMessage(): LiveData<String> {
        return repo.getMessage()
    }

    override fun getDataFromRetrofit(): LiveData<HeadlineResponse> {
        data = repo.loadData()
        return data
    }
}