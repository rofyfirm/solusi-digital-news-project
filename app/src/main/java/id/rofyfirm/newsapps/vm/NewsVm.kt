package id.rofyfirm.newsapps.vm

import androidx.lifecycle.LiveData
import id.rofyfirm.newsapps.base.BaseVm
import id.rofyfirm.newsapps.repo.NewsRepo
import id.rofyfirm.newsapps.response.NewsDetailResponse

class NewsVm(private val repo: NewsRepo):
    BaseVm<NewsDetailResponse>(){

    private lateinit var data: LiveData<NewsDetailResponse>

    override fun getStatusMessage(): LiveData<String> {
        return repo.getMessage()
    }

    override fun getDataFromRetrofit(): LiveData<NewsDetailResponse> {
        data = repo.loadData()
        return data
    }
}