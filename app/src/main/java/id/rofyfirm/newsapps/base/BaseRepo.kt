package id.rofyfirm.newsapps.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import id.rofyfirm.newsapps.network.RequestApi
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response

abstract class BaseRepo <T>(
    @PublishedApi internal val request: RequestApi
) {

    abstract fun loadData(): LiveData<T>

    val message = MutableLiveData<String>()

    inline fun <reified T : Any> fetchData(crossinline call: (RequestApi) -> Deferred<Response<T>>): LiveData<T> {
        val result = MutableLiveData<T>()

        CoroutineScope(Dispatchers.IO).launch {
            val request = call(request)
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
                        result.value = response.body()
                        message.value = null
                    } else {
                        result.value = null
                        message.value = "Error ${response.code()} : ${response.message()}"
                    }
                } catch (e: HttpException) {
                    Log.d("Error", "Error: ${e.message()}")
                } catch (e: Throwable) {
                    Log.d("Error", "Error: ${e.message}")
                }
            }
        }

        return result
    }

    open fun getMessage(): LiveData<String> {
        return message
    }
}