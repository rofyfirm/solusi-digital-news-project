package id.rofyfirm.newsapps.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

abstract class BaseVm <T>: ViewModel() {
    abstract fun getStatusMessage(): LiveData<String>
    abstract fun getDataFromRetrofit(): LiveData<T>
}