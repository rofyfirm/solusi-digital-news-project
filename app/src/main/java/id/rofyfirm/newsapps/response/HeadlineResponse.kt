package id.rofyfirm.newsapps.response

import com.google.gson.annotations.SerializedName

class HeadlineResponse {
    @SerializedName("status") val status: String? = null
    @SerializedName("totalResults") val totalResults: Long? = null
    @SerializedName("articles") val articles: List<Article>? = null

    data class Article(
        @SerializedName("source") val source: Source,
        @SerializedName("author") val author: String,
        @SerializedName("title") val title: String,
        @SerializedName("description") val description: String,
        @SerializedName("url") val url: String,
        @SerializedName("urlToImage") val urlToImage: String,
        @SerializedName("publishedAt") val publishedAt: String,
        @SerializedName("content") val content: String
    )

    data class Source(
        @SerializedName("id") val id: Any? = null,
        @SerializedName("name") val name: String
    )
}
