package id.rofyfirm.newsapps.di

import android.app.Application
import id.rofyfirm.newsapps.networkModule
import id.rofyfirm.newsapps.repositoryModule
import id.rofyfirm.newsapps.vmModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class AppConfig: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger(Level.NONE)
            androidContext(this@AppConfig)
            modules(listOf(networkModule, repositoryModule, vmModule))
        }
    }
}