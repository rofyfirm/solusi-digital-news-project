package id.rofyfirm.newsapps

import id.rofyfirm.newsapps.network.ApiClient
import id.rofyfirm.newsapps.repo.MainRepo
import id.rofyfirm.newsapps.repo.NewsRepo
import id.rofyfirm.newsapps.utils.ConnectivityInterceptor
import id.rofyfirm.newsapps.vm.MainVm
import id.rofyfirm.newsapps.vm.NewsVm
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val networkModule = module {
    single { ConnectivityInterceptor(get()) }
    single { ApiClient().retrofit(get(), androidContext())}
}

val repositoryModule = module {
    single { MainRepo(get()) }
    single { NewsRepo(get()) }
}

val vmModule = module {
    single { MainVm(get()) }
    single { NewsVm(get()) }

}
