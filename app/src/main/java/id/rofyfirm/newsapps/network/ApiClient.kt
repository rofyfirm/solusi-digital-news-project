package id.rofyfirm.newsapps.network

import android.annotation.SuppressLint
import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import id.rofyfirm.newsapps.BuildConfig
import id.rofyfirm.newsapps.utils.ConnectivityInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    companion object{
        private fun connectivity(connectivityInterceptor: ConnectivityInterceptor): OkHttpClient {
            return OkHttpClient.Builder().addInterceptor(connectivityInterceptor).build()
        }

    }

    @SuppressLint("ServiceCast")
    private fun header(context: Context): OkHttpClient {
        val builder = OkHttpClient().newBuilder()
        builder.readTimeout(120, TimeUnit.SECONDS)
        builder.connectTimeout(120, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            builder.addInterceptor(interceptor)
        }

        builder.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader(
                    "Authorization", "01b51eec27e147be834fe67235d957cc"
                )
                .build()
            chain.proceed(request)
        }

        return builder.build()
    }

    fun retrofit(connection: ConnectivityInterceptor, context: Context): RequestApi {
        return Retrofit.Builder()
            .baseUrl("https://newsapi.org/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(connectivity(connection))
            .client(header(context))
            .build()
            .create(RequestApi::class.java)
    }
}