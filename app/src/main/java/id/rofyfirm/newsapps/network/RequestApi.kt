package id.rofyfirm.newsapps.network

import id.rofyfirm.newsapps.response.HeadlineResponse
import id.rofyfirm.newsapps.response.NewsDetailResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RequestApi {

    @GET ("top-headlines")
    fun topHeadlines(
        @Query("country") country: String?
    ): Deferred<Response<HeadlineResponse>>

    @GET("")
    fun detailNews(

    ): Deferred<Response<NewsDetailResponse>>
}